/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React  from 'react';
import { StyleSheet, Text, View} from 'react-native';

import { Provider } from 'react-redux';
import Store from './components/store';
import {Header,CryptoContainer } from './components';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={Store}>
      <View>
      <Header/>
      <CryptoContainer/>
      </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
