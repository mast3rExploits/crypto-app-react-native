import React from 'react';
import {View, Text, StyleSheet, Image } from 'react-native';
import { images } from './CoinIcon'

const styles = StyleSheet.create({
    container:{
        display: "flex",
        marginBottom: 20,
        borderBottomColor: '#e5e5e5',
        borderBottomWidth: 3,
        padding: 20
    },
    upperRow:{
        display: "flex",
        flexDirection: "row",
        marginBottom: 15,
    },
    sparator:{
        marginTop:10,
    },
    coinSymbol:{
        marginTop: 10,
        marginRight: 5,
        marginLeft: 20,
        fontWeight: "bold"
    },
    namaCoin:{
        marginTop: 10,
        marginRight: 20,
        marginLeft: 10,
    },
    price:{
        marginTop: 10,
        marginRight: 10,
        marginLeft: "auto",
        fontWeight: "bold"
    },
    image:{
        height: 40,
        width: 40,
    },
    change:{
        display: "flex",
        borderTopColor: "#FAFAFA",
        borderTopWidth: 2,
        padding: 10,
        flexDirection: "row",
        justifyContent:"space-around"
    },
    bold:{
        fontWeight:"bold",
    },
    plus:{
        fontWeight:"bold",
        color:"#00BFA5",
        marginLeft:5
    },
    minus:{
        fontWeight:"bold",
        color:"#DD2C00",
        marginLeft:5
    }
})

const {container, image, bold, upperRow, coinSymbol, sparator, namaCoin, price,change, plus, minus} = styles;

const CoinCard = ({symbol, coin_name, price_usd, percent_change_24h, percent_change_7d}) => {
    return (
        <View style={container}>
            <View style={upperRow}>
                <Image
                    style={styles.image}
                    source={{uri:images[symbol]}}
                />
                <Text style={coinSymbol}>{symbol}</Text>
                <Text style={sparator}>|</Text>
                <Text style={namaCoin}>{coin_name}</Text>            
                <Text style={price}>{price_usd}
                <Text style={bold}>$</Text>
                </Text>
                </View>
                <View style={change}>
                    <Text>24H :
                    <Text style={ percent_change_24h < 0 ? minus : plus }>{percent_change_24h}</Text>
                    </Text>
                    <Text>7D :
                    <Text style={ percent_change_24h < 0 ? minus : plus }>{percent_change_7d}</Text>
                    </Text>
            </View>
        </View>
    )
}

export default CoinCard;