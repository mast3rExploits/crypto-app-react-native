import { combineReducers } from 'redux';
import CryptoReducers from './CriptoReducer';

export default combineReducers({
    crypto : CryptoReducers,
})